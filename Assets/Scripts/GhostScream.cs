﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GhostScream : MonoBehaviour {
    Animator Anim;
    NavMeshAgent agent;

    const string Enemy_Tag = "Enemy";

    private Transform Player;
    private float currentTime;
    private float coolDown = 3.0f;
    private AudioSource audi;
    bool standing = false;

    bool scared = false;

    int scaredHash = Animator.StringToHash("scared");

    RaycastHit hit;
    // Use this for initialization
    void Start () {
        currentTime = Time.time;
        audi = GetComponent<AudioSource>();
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        //Vector3 looking = new Vector3(Player.position.x, 0, Player.position.z);
        if (OVRInput.Get(OVRInput.Button.One) && Time.time > coolDown + currentTime)
        {
            audi.Play();
            currentTime = Time.time;

            if (hit.collider.tag == Enemy_Tag && OVRInput.Get(OVRInput.Button.One))
            {
                agent.isStopped = true;
                //transform.LookAt(looking);
                standing = false;
                scared = true;
            }
            else
            {
                standing = true;
                agent.isStopped = false;
                scared = false;
            }
        }
    }
}
