﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartScript : MonoBehaviour {

    private string prevScene;


    //public string LastScene (string scene)
    //{
    //    prevScene = scene;
    //}

    // Use this for initialization
    void Start()
    {
        prevScene = PlayerPrefs.GetString("PrevScene");
    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.One))
        {
            Debug.Log("A Pressed.");
            SceneManager.LoadScene(prevScene);
        }

        if (OVRInput.GetDown(OVRInput.Button.Two))
        {
            Debug.Log("B Pressed.");
            SceneManager.LoadScene("MainMenu");
        }
    }
}
