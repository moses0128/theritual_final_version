﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeFire : MonoBehaviour {

    public Transform[] firestar;
    public GameObject firewall;
    int currentWaypointIndex = 0;
    private int frameCount = 0;
    private int hits = 0;


    public void Damage(int num)
    {
        hits = num;
    }

    void Start () {
		
	}
	
	
	void Update () {

        frameCount += 1;

		if(frameCount >= 60 * 4)
        {
            Instantiate(firewall, firestar[currentWaypointIndex].position, firestar[currentWaypointIndex].rotation);
            if(hits >= 1)
            {
                Instantiate(firewall, firestar[(currentWaypointIndex + 1) % firestar.Length].position, firestar[(currentWaypointIndex + 1) % firestar.Length].rotation);
            }
            if (hits >= 2)
            {
                Instantiate(firewall, firestar[(currentWaypointIndex + 2) % firestar.Length].position, firestar[(currentWaypointIndex + 2) % firestar.Length].rotation);
            }
            currentWaypointIndex = (currentWaypointIndex + 1) % firestar.Length;
            frameCount = 0;
        }
	}
}
