﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pushBack : MonoBehaviour {

    public Transform entrance;
    public PlayerKill pk;
    private Vector3 direction;
    private float frameCount = 0;

    private bool push = false;
    private bool pushed = false;

    private Vector3 stop;
    


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        direction = entrance.position - transform.position;

        if (push == true)
        {
            stop = direction;
            GetComponent<Rigidbody>().AddForce(direction, ForceMode.VelocityChange);
            Debug.Log("Push.");
            push = false;
            pushed = true;
        }

        if (pushed == true)
        {
            frameCount++;
        }

        if (frameCount >= 60 * 1.3f)
        {
            pushed = false;
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            frameCount = 0;
            pk.Killswitch(true);
        }
        Debug.Log(frameCount);

    }

    public void Reset()
    {
        push = true;
        pk.Killswitch(false);
    }
}
