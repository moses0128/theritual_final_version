﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeChannel : MonoBehaviour
{
    public GameObject[] message = new GameObject[10];
    private int x = 0;
    private float coolDown = 2.0f;
    private float currentTime;
    public GameObject remote;
    private Vector3 remoteStart;
    private AudioSource aud;
    

    // Use this for initialization
    void Start ()
    {
        currentTime = Time.time;
        remoteStart = new Vector3(remote.transform.position.x , remote.transform.position.y , remote.transform.position.z);
        aud = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        Debug.Log(remote.transform.position + " " + remoteStart);
        if (remote.transform.position != remoteStart)
        {
            if (OVRInput.Get(OVRInput.Button.One) && Time.time > coolDown + currentTime)
            {
                aud.Play();
                message[x].SetActive(false);
                x++;
                if (x > 10)
                {
                    x = 0;
                }
                message[x].SetActive(true);
                currentTime = Time.time;
            }
            else if (OVRInput.Get(OVRInput.Button.Two) && Time.time > coolDown + currentTime)
            {
                aud.Play();
                message[x].SetActive(false);
                x--;
                if (x < 0)
                {
                    x = 10;
                }
                message[x].SetActive(true);
                currentTime = Time.time;
            }
        }
	}
}


