﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetalDestroy : MonoBehaviour
{
    const string bullet_tag = "Projectile";
    const string enemy_tag = "Enemy";

    private int bullet_count;
    //private AudioSource aud;

    // Use this for initialization
    void Start()
    {
        bullet_count = 0;
        //aud = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(bullet_count);
        if (bullet_count == 4)
        {
            //aud.Play();
            Destroy(gameObject, 2.0f);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log(collision.gameObject.tag);
        if (collision.gameObject.tag == bullet_tag)
        {
            //bullet_count += 1;
        }
        if (collision.gameObject.tag == enemy_tag)
        {
            //aud.Play();
            Destroy(gameObject, 0.0f);
        }
    }
}
