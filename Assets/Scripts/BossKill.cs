﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class BossKill : MonoBehaviour {
    const string throw_tag = "Throw";
    private bool dead = false;
    private int framecount = 0;
    private int hits = 0;
    public MakeFire genFire;
    public pushBack pb;
    public AudioSource aud;
    public AudioSource aud1;

    // Use this for initialization
    void Start () {
        //aud = GetComponent<AudioSource>();
        //aud1 = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        if (dead == true)
        {
            framecount += 1;
        }

        if(framecount >= (60 * 5))
        {
            SceneManager.LoadScene("Main Menu");
        }

        if(hits >= 3)
        {
            dead = true;
            GetComponent<BossFire>().enabled = false;
            SceneManager.LoadScene("Ending");
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log(collision.gameObject.tag);
        if (collision.gameObject.tag == "metal")
        {
            aud.Play();
            aud1.Play();
            hits++;
            genFire.Damage(hits);
            pb.Reset();
            Debug.Log("Hit.");
        }
    }
}
