﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LoadTutorial : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(OVRInput.Get(OVRInput.Button.One))
        {
            Debug.Log("A Pressed.");
            SceneManager.LoadScene("Ritual Tutorial");
        }

        if (OVRInput.Get(OVRInput.Button.Two))
        {
            Debug.Log("B Pressed.");
            SceneManager.LoadScene("Main");
        }

        Debug.Log(OVRInput.Get(OVRInput.Button.One));
        Debug.Log("Test.");
	}
}
