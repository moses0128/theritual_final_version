﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Patrolling : MonoBehaviour
{
    
    
    public Transform[] waypoint;
    private int frameCount = 0;
    
    public AudioSource aud;
    

    NavMeshAgent agent;
    Animator Anim;
    bool walking = true;
    bool idle = false;
    
    int walkHash = Animator.StringToHash("walking");
    int idleHash = Animator.StringToHash("idle");
    

    int currentWaypointIndex = 0;
    const int EPSILON = 2;

    // Use this for initialization
    void Start ()
    {
        Anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        //agent.SetDestination(waypoint[currentWaypointIndex].position);
        currentWaypointIndex = 0;
        agent.SetDestination(waypoint[currentWaypointIndex].position);
        
    }

    // Update is called once per frame
    void Update ()
    {

        //Debug.Log(agent.speed);
        //Debug.Log((transform.position - waypoint[currentWaypointIndex].position).magnitude);
        if ((transform.position - waypoint[currentWaypointIndex].position).magnitude < EPSILON)
        {
            walking = false;
            aud.Stop();
            idle = true;
            
            frameCount += 1;
            if (frameCount >= (60 * 4))
            {
                currentWaypointIndex = (currentWaypointIndex + 1) % waypoint.Length;
                agent.SetDestination(waypoint[currentWaypointIndex].position);
                //agent.SetDestination(waypoint[Random.Range(0, waypoint.Length)].position);
                frameCount = 0;
                walking = true;
                aud.Play();
                idle = false;
            }
        }

        Anim.SetBool(walkHash, walking);
        Anim.SetBool(idleHash, idle);
    }
}
