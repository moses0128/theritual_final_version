﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BossMove : MonoBehaviour
{

    public bool fightStart = false;
    private bool arrived = false;
    public Transform[] waypoint;
    private int frameCount = 0;

    NavMeshAgent agent;
    Animator Anim;
    bool walking = true;
    bool idle = false;
    int walkHash = Animator.StringToHash("walking");
    int idleHash = Animator.StringToHash("idle");

    int currentWaypointIndex = 0;
    const int EPSILON = 2;
    private Transform Player;

    public void FightStart()
    {
        fightStart = true;
        currentWaypointIndex = 2;
        agent.SetDestination(waypoint[2].position);
    }

    // Use this for initialization
    void Start()
    {
        Anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        //agent.SetDestination(waypoint[currentWaypointIndex].position);
        currentWaypointIndex = 0;
        agent.SetDestination(waypoint[currentWaypointIndex].position);
        Player = GameObject.Find("Player").transform;

    }

    // Update is called once per frame
    void Update()
    {
        Vector3 looking = new Vector3(Player.position.x, 0, Player.position.z);
        //Debug.Log(fightStart);
        //Debug.Log("Idle: " + idle);
        //Debug.Log("Stopped: " + agent.isStopped);
        //Debug.Log("Index: " + currentWaypointIndex);

        //Debug.Log(agent.speed);
        //Debug.Log((transform.position - waypoint[currentWaypointIndex].position).magnitude);
        if ((transform.position - waypoint[currentWaypointIndex].position).magnitude < EPSILON)
        {
            walking = false;
            idle = true;
            frameCount += 1;
            if (frameCount >= (60 * 10) && fightStart == false)
            {
                currentWaypointIndex = (currentWaypointIndex + 1) % (waypoint.Length - 1);
                agent.SetDestination(waypoint[currentWaypointIndex].position);
                //agent.SetDestination(waypoint[Random.Range(0, waypoint.Length)].position);
                frameCount = 0;
                walking = true;
                idle = false;
            }


            if (fightStart == true && idle == true)
            {
                arrived = true;
                transform.LookAt(looking);
                //agent.isStopped = true;
            }

        }

        if (fightStart == true && arrived == false)
        {
            frameCount = 0;
            walking = true;
            idle = false;
        }

        Anim.SetBool(walkHash, walking);
        Anim.SetBool(idleHash, idle);
    }
}
