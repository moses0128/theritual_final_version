﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BossFire : MonoBehaviour
{

    public Rigidbody bullet;
    // public GameObject trail;
    private Transform barrelEnd;
    public float distance = 10f;
    const string Player_Tag = "Player";

    private Transform Player;
    private int bulletCount = 30;
    private int frameCount = 0;
    private Vector3 direction;
    NavMeshAgent agent;
    Animator Anim;


    bool firing = true;
    bool seePlayer = false;
    bool walking = true;
    bool idle = false;
    bool standing = false;
    int walkHash = Animator.StringToHash("walking");
    int idleHash = Animator.StringToHash("idle");
    int standHash = Animator.StringToHash("standing");

    //public Transform Enemy;


    //Vector3 direction;
    RaycastHit hit;


    // Use this for initialization
    void Start()
    {
        Anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        barrelEnd = gameObject.transform.Find("Armature/Hips/Spine/Chest/Shoulder.R/UpperArm.R/LowerArm.R/Hand.R/Gun/GunEnd");
        Player = GameObject.Find("Player").transform;
        //walking = Anim.GetBool(walkHash);
        //idle = Anim.GetBool(idleHash);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 looking = new Vector3(Player.position.x, 0, Player.position.z);
        Debug.DrawRay(transform.position, looking);
        direction = Player.position - barrelEnd.position;
        //transform.LookAt(Player);
        //Debug.DrawRay(barrelEnd.position, direction);
        //Debug.Log (firing);


        if (Physics.Raycast(barrelEnd.position, direction, out hit, distance))
        {
            if (hit.collider.tag == Player_Tag)
            {
                agent.isStopped = true;
                transform.LookAt(looking);
                standing = true;

                if (firing == true)
                {
                    frameCount += 1;


                    //Debug.Log(frameCount);
                }


                if (frameCount >= (60 * 1.5f))
                {
                    firing = false;
                    frameCount = 0;
                }


                if (firing == false && bulletCount > 0)
                {

                    firing = true;
                    Rigidbody bulletInstance;
                    bulletInstance = Instantiate(bullet, barrelEnd.position, barrelEnd.rotation) as Rigidbody;
                    //Super messy ramdomization for the bullet direction below.
                    bulletInstance.AddForce((direction) * 100);
                    bulletCount -= 1;

                }
            }
            else
            {
                standing = false;
                agent.isStopped = false;
                frameCount = 0;
            }
            Anim.SetBool(standHash, standing);
        }

    }
}

