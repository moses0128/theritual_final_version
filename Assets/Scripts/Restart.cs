﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour {

    private string prevScene = PlayerPrefs.GetString("PrevScene");


    //public string LastScene (string scene)
    //{
    //    prevScene = scene;
    //}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (OVRInput.Get(OVRInput.Button.One))
        {
            Debug.Log("A Pressed.");
            SceneManager.LoadScene(prevScene);
        }

        if (OVRInput.Get(OVRInput.Button.Two))
        {
            Debug.Log("B Pressed.");
            SceneManager.LoadScene("Main Menu");
        }
    }
}
