﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Unlock : MonoBehaviour {


    public GameObject door;
    private AudioSource audi;



    // Use this for initialization
    void Start ()
    {
        audi = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            audi.Play();
            Destroy(door);
            Destroy(gameObject, 1.0f);
        }
    }
}
