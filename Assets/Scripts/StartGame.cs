﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class StartGame : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (OVRInput.GetDown(OVRInput.Button.One))
        {
            Debug.Log("A Pressed.");
            SceneManager.LoadScene("Ritual Tutorial");
        }

        if (OVRInput.GetDown(OVRInput.Button.Two))
        {
            Debug.Log("B Pressed.");
            SceneManager.LoadScene("Main");
        }

        
    }
}