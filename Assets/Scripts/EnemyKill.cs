﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyKill : MonoBehaviour
{
    public AudioSource aud;
    private NavMeshAgent agent;
    private Animator anim;

    const string throw_tag = "Throw";

	// Use this for initialization
	void Start ()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == throw_tag)
        {
            aud.Play();
            GetComponent<EnemyFire>().enabled = false;
            agent.enabled = false;
            anim.enabled = false;
            Destroy(gameObject, 3.0f);
        }
    }
}
