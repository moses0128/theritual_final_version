﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerKill : MonoBehaviour {


    const string bullet_tag = "Projectile";

    private bool canDie = true;
    
    public void Killswitch(bool Ks)
    {
        canDie = Ks;
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider collision)
    {
        //Debug.Log(collision.gameObject.tag);
        if (collision.gameObject.tag == bullet_tag && canDie == true)
        {
            //Debug.Log("Im not off jaja");
            PlayerPrefs.SetString("PrevScene", SceneManager.GetActiveScene().name);
            SceneManager.LoadScene("Dead");
            //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            //Debug.Log(SceneManager.GetActiveScene().name);
        }
    }
}
